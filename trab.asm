%include "../asm_io.inc"

%define EMPTY 0
%define WHITE 1
%define BLACK 6 
%define SIZE 5 

%macro sum_board 1      ; macro para somar todos os elementos de seu parametro, retorna em ax
; altera regs: ax, ecx

        mov esi, %1                    ; move primeiro parametro em esi
        mov ax, 0
        mov ecx, SIZE 
        %%LP:
        add ax, [esi+(ecx-1)*2]        ; indice do vetor vai de 0 a SIZE - 1
        loop %%LP
%endmacro

%macro push_board 1     ; macro para empilhar SIZE elementos de seu parametro
; alter regs: ax, ecx

        mov ecx, 0
        %%LP:
        mov ax, [%1+ecx*2]      
        push ax
        inc ecx
        cmp ecx, SIZE 
        jne %%LP
%endmacro

%macro pop_board 1      ; macro para desempilhar SIZE elementos de seu parametro  
; alter regs: ax, ecx

        mov ecx, 5 
        %%LP:
        pop ax
        mov [%1+(ecx-1)*2], ax  ; movendo o elemento desempilhado para o tabuleiro (de tras para frente)
        loop %%LP
%endmacro


%macro do_switch 2   ; macro para fazer a troca de de uma peca (preto -> branco, branco -> preto, vazio -> vazio)
; altera regs: 
; parametro %1 e o vetor, parametro %2 e a posicao onde sera realizada a troca 

        cmp word [%1+%2*2], BLACK
        je %%L1
        cmp word [%1+%2*2], WHITE
        jne %%end

        mov word [%1+%2*2], BLACK    ; caso branco 
        jmp %%end

        %%L1:
        mov word [%1+%2*2], WHITE    ; caso preto
        %%end:
%endmacro

segment .data
        board dw 0, BLACK, BLACK, BLACK, BLACK, BLACK, 0   ; vetor com 7 pos para nao precisar tratar especialmente troca na primeira e ultima posicao
        win_count dw 0          ; contador de vitorias

segment .text
        global asm_main

asm_main:
        push ebp
        mov ebp, esp
        
        mov esi, board
        add esi, 2   ; abstraindo o fato de que o vetor tem 7 posicoes, pulando os dois primeiros bytes(word)
        push esi     ; parametro da funcao play
        call play
        add esp, 4
        
        movzx eax, word [win_count]
        call print_int

        leave
        ret

play:
;       funcao play realiza as jogada nas pecas pretas do tabuleiro
;       args: board address
;       local variables: board components

        push ebp
        mov ebp, esp
        mov esi, [ebp+8]
                
        sum_board esi ; somando o tabuleiro para saber se a configuracao atual e vencedora, perdedora ou nao-final

        cmp ax, 0
        je END_WIN    ; se existem somente pecas vazias (valor 0), configuracao vencedora
        cmp ax, 5
        jle END_LOSE  ; senao se nao existam pecas pretas (valor 6), configuracao perdedora
                      
                      ; senao se existem pecas pretas (valor 6), configuracao nao-final

        mov edx, 0

        LP0:
                cmp word [esi+edx*2], BLACK     
                jne cont_lp0 
                                                ; caso peca seja preta
                push_board esi                  ; armazena o tabuleiro na pilha 
                push edx                        ; armazena posicao da peca preta econtrada

                sub word [esi+edx*2], BLACK   ; elimina a peca preta
;                do_switch esi, edx            ; 
                sub edx, 1
                do_switch esi, edx            ; troca peca da esquerda
                add edx, 2 
                do_switch esi, edx            ; troca peca da direita 
                sub edx, 1

                push esi                      ; parametro da funcao play
                call play
                add esp, 4                    ; remove parametro da funcao da pilha

                pop edx                         ; recupera posicao da ultima peca preta encontrada
                pop_board esi                   ; recupera ultima configuracao do tabuleiro

                cont_lp0:
                inc edx
                cmp edx, SIZE 
                jl LP0
                
                jmp END_LOSE

        END_WIN:
        inc word [win_count]                    ; incrementa contador de vitoria

        END_LOSE:                               
        leave
        ret
